<?php

if (!function_exists('metatags')) {
    /**
     * @return \Blok\Metatags\Metatags|\Illuminate\Foundation\Application|Metatags|mixed
     */
    function metatags()
    {
        return app('metatags');
    }
}

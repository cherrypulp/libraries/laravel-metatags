<?php

namespace Blok\Metatags;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/metatags.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('metatags.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/metatags'),
        ], 'views');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'metatags');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'metatags'
        );

        $this->app->singleton('metatags', function () {
            return new Metatags();
        });

        require_once __DIR__ . '/helpers.php';
    }
}

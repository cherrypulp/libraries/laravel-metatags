<?php

namespace Blok\Metatags\Facades;

use Illuminate\Support\Facades\Facade;

class Metatags extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'metatags';
    }
}

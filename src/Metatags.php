<?php

namespace Blok\Metatags;

use Blok\Metatags\Traits\HasMetatags;
use Illuminate\Support\Collection;

class Metatags extends Collection
{
    use HasMetatags;

    public $entity;

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity ?? $this;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * Get all metatags
     *
     * @param array $override
     * @return array|void
     */
    public function all($override = []){
        return array_merge($this->getMetatags(), $this->getEntity()->getMetatags(), $override);
    }

    /**
     * Transform meta into array
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function toHtml(){
        return view('metatags::meta')->with('metas', $this->all());
    }
}

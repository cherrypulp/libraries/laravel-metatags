<?php

namespace Blok\Metatags\Tests;

use Blok\Metatags\Facades\Metatags;
use Blok\Metatags\ServiceProvider;
use Orchestra\Testbench\TestCase;

class MetatagsTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'metatags' => Metatags::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}

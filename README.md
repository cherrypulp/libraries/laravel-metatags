# Metatags

[![Build Status](https://travis-ci.org/blok/metatags.svg?branch=master)](https://travis-ci.org/blok/metatags)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/blok/metatags/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/blok/metatags/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/blok/metatags/badge.svg?branch=master)](https://coveralls.io/github/blok/metatags?branch=master)

[![Packagist](https://img.shields.io/packagist/v/blok/metatags.svg)](https://packagist.org/packages/blok/metatags)
[![Packagist](https://poser.pugx.org/blok/metatags/d/total.svg)](https://packagist.org/packages/blok/metatags)
[![Packagist](https://img.shields.io/packagist/l/blok/metatags.svg)](https://packagist.org/packages/blok/metatags)

## Package description:

Metatags helper for Laravel. Handle the cumbersome common metatags with a more fluent way of integrate that in your projet.

## Installation

Install via composer
```bash
composer require blok/metatags
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Blok\Metatags\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Blok\Metatags\Facades\Metatags::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Blok\Metatags\ServiceProvider" --tag="config"
```

## Usage

To use the Metatags helpers, simply add to your model this Trait.

```php
<?php

class User extends \Illuminate\Database\Eloquent\Model {
    use \Blok\Metatags\Traits\HasMetatags;
}
```

## How to define a metatags ?

To add a default metatags through your entity => you can override the getMetatags() function like that.

```php
<?php

class User extends \Illuminate\Database\Eloquent\Model {

    use \Blok\Metatags\Traits\HasMetatags;

    public function getMetatags($override = []){
        $this
            ->setMetaTitle($this->name)
            ->setMetaDescription($this->description)
            ->setMetaKeywords($this->keywords);
        return $this->formatMetatags($override);
    }
}
```

## How to add the metatags in your html ?

In your blade file, you can simply call this views.

```php
@include('metatags.meta', $user->getMetatags())
```

## Using the global Metatags Facade

Likely, you will most of the times put the meta in a main layouts template. There is a little helper who can help you with that.

In your /layouts/html for exemple :

```php
@include('metatags.meta', app('metatags')->all())
```

Then in your controller, you must define the entity you want retrieve the metatags informations like that.

````php
app('metatags')->setEntity($user);
````

You are also free to setup some metatags by default or common metatags likely in your controller or AppServiceProvider like that :

````php
app('metatags')->setTitle('Default title');
````

## Override default metatags

In some case you will also need to override the default metatags from an entity. That's why when you output the metatags, you can also add your metatags like that.

```php
@include('metatags.meta', app('metatags')->getMetatags(['title' => 'This will override the default one']))
```

## Order of metatags definition

1. Global defined metatags
2. Entity defined metatags
3. Override metatags

## Metatags fallback

It could be also cumbersome to define default metatags fallback when you want to use the same variable than an other metatags.

That's why they are some default fallback (for instance using the metatags title for the og:title when there is no og:title defined) included in the package.

You can override them by exporting the config.php and don't hesitate to propose yours !.

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- [Daniel Sum](https://github.com/blok/metatags)
- [All contributors](https://github.com/blok/metatags/graphs/contributors)

This package is bootstrapped with the help of
[cherrypulp/laravel-package-generator](https://github.com/cherrypulp/laravel-package-generator).
